package kwic_hw3;
import java.util.ArrayList;
import java.util.Arrays;

public class circular_shifter extends Subject implements Observer {
private ArrayList<String>  shifts;


//method to circular shift each line
void setup(ArrayList<String> a){
	
	shifts=new ArrayList<String>();
	
	for(int i=0;i<a.size();i++){
		
		shift(a.get(i));
	}
	}

 ArrayList<String>  get_lines(){
	return shifts;
	
	
}
 
 // shift every line private function
private void shift(String line){

	String[] splited = line.split(" ");
	
	shifts.add(line);
	for(int i=0;i<splited.length-1;i++){
		
			String first = splited[0];
			System.arraycopy(splited, 1, splited, 0, splited.length-1);
			splited[splited.length-1] = first;
			String temp = Arrays.toString(splited);
		
			temp = temp.replaceAll(",","");
		
			temp = temp.substring(1, temp.length()-1);
			
			shifts.add(temp);
			
		}
	}


@Override
public void updateEvent(EventClass e) {
	

	setup(e.getlines());
	
	e.addlines(shifts);
	
	for (int i = 0; i < observers.size(); i++) {
		Observer listener = observers.get(i);
		listener.updateEvent(e);
	}
	
	
}


		
		
		
	
	
	
	
	
	
}

