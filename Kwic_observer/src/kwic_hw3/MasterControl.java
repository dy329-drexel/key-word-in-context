package kwic_hw3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MasterControl {


	//creating subject and observers
	
	static line_storage kwic=new line_storage();
	
	
	static alphabetizer alSubject=new alphabetizer();
	
	static console_input inp=new console_input();

	
	static circular_shifter sf=new circular_shifter();
	
	

		
		
	
	
	public static void main(String args[]){
		
		 System.out.println("Valid commands are a,d,p,q");
	
	
		BufferedReader s= new BufferedReader(new InputStreamReader(System.in));
		
		//adding observers 
	    kwic.addObserver(sf);
	    
	    sf.addObserver(alSubject);
	   
	    
	    //running menu 
		String a;
		try {
			  while(true){  
				 
					System.out.println("Add, Delete, Print, Quit: ");
				 a = s.readLine();
				if(a.equals("a")){
					inp.getinput(kwic);
					
				}
				else 
				if(a.equals("d")){
					alSubject.del_line(s.readLine());
				}
				else 
					if(a.equals("p")){
					alSubject.print();
						
						
					}
					else 
						if(a.equals("q")){
							System.exit(0);
						}
				else
				{
					System.out.println("enter valid input");
				}
				
			}} catch (NumberFormatException e) {
				
				main(null);
			
			} catch (IOException e) {
				
				e.printStackTrace();
				main(null);
			}
		
	}
}
