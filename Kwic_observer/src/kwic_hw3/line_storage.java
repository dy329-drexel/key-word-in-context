package kwic_hw3;

import java.awt.event.ActionListener;
import java.util.ArrayList;

public class line_storage extends Subject{



	 ArrayList<String> lines=new ArrayList<String>();
	
	
	public String get_line(int a) {
		return lines.get(a);
		
	}
  public void add_line(String b) {
		lines.add(b);
		
		notifyObserver();
		
	}
  public void del_line(String b) {
		lines.remove(b);
		notifyObserver();
	}

public line_storage get_lines() {
		return this;
	}

	
	public int getlength() {
		return lines.size();
	}

	public void notifyObserver(){
		EventClass e=new EventClass();
		e.addlines(lines);
		
		for (int i = 0; i < observers.size(); i++) {
			Observer listener = observers.get(i);
			listener.updateEvent(e);
		}
		
		
		
	}

}
