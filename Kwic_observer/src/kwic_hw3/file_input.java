package kwic_hw3;
import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;

import java.util.Scanner;

public class file_input implements input_strat{


	line_storage storage =new line_storage();
	
	//input from file
	@Override
	public void getinput() {
	System.out.println("enter the name of the file ");
	
	
	Scanner input = new Scanner(System.in);
    String file_name=input.nextLine();
    
   
    FileReader fileReader;
	try {
		fileReader = new FileReader(new File(file_name+".txt"));
		
		BufferedReader br = new BufferedReader(fileReader);

	    String line = null;
	    // if no more lines the readLine() returns null
	    while ((line = br.readLine()) != null) {
	         // reading lines until the end of the file
	    	storage.add_line(line);
		       System.out.println("added line "+line);
	    }
		
		
		
		
	} catch (Exception e) {
		
		e.printStackTrace();
	}
 
    
		
	}

	@Override
	public void getinput(line_storage kwic) {
		// TODO Auto-generated method stub
		
	}
	
	
}
