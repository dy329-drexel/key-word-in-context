import java.util.List;
import java.util.Scanner;

public class console_input implements input_strat {

	
	line_storage storage =new string_storage();
	
	//take console input
	@Override
	public void getinput() {
	System.out.println("enter the input");
	
	
	Scanner input = new Scanner(System.in);
    
    String lineNew;

    while (input.hasNextLine()) {
        lineNew = input.nextLine();
        if (lineNew.isEmpty()) {
            break;
        }
       storage.add_line(lineNew);
       System.out.println("added line "+lineNew);
    }
		
	}
	@Override
	public line_storage getStorage() {
	
		return storage;
	}
	

}
