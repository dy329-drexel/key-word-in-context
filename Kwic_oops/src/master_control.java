import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class master_control {

	
	//objects for all modules
	static input_strat kwic=null;
	
	static output_strat outmed=new file_out();
	static circular_shifter sf=new circular_shifter();
	static alphabetizer al=new alphabetizer();
	
	// common menu for input output
	static void menu(){
		System.out.println("1. for console");
		System.out.println("2. for text file");
		System.out.println("any other key to quit");
		
	}
	
	public static void main(String args[]){
		
		
		//taking choice of input from user
		System.out.println("Enter the method for input");
		menu();
		BufferedReader s= new BufferedReader(new InputStreamReader(System.in));
	    try {
	    
			int a = Integer.parseInt(s.readLine());
			if(a==1){
				kwic=new console_input();
			}
			else 
			if(a==2){
				kwic=new file_input();
			}
			else
			{
				System.exit(0);
			}
			
		} catch (NumberFormatException e) {
			System.out.println("enter valid input");
			main(null);
		
		} catch (IOException e) {
			
			e.printStackTrace();
			main(null);
		}
	    
	    //passing the read input through cicular shift and alphabetizer module

		kwic.getinput();
		sf.setup(kwic.getStorage());
		al.alphabetizer(sf);
	
		//choice of output from user
		System.out.println("Enter the method for output");
		menu();
		 try {
			    
				int a = Integer.parseInt(s.readLine());
				if(a==1){
					outmed=new console_out();
				}
				else 
				if(a==2){
					outmed=new file_out();
				}
				else
				{
					System.exit(0);
				}
				
			} catch (NumberFormatException e) {
				System.out.println("enter valid input");
				main(null);
			
			} catch (IOException e) {
				
				e.printStackTrace();
				main(null);
			}
		 outmed.print(al);
		main(null);
	}
	}
