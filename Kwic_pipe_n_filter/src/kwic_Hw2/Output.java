package kwic_Hw2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Output extends Filter{

	private String outputfile;
	public Output(Pipe input_, Pipe output_) {
		super(input_, output_);
		
	}

	@Override
	protected void transform() {
		ArrayList<String> data=new ArrayList<String>();
		try {
			
			//getting all the lines and printing to the file given by user
	
			String line;
			while ((line=input_.get().toString())!= "STOP351786548775GYUFABJFFAUYRJB"){
				
				data.add(line);
				
			}
		
			Path file = Paths.get(outputfile);
			Files.write(file, data, Charset.forName("UTF-8"));
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		
	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


//getting filename passed by user
	public void setOutputfile(String outputfile) {
		this.outputfile = outputfile;
	}

}
