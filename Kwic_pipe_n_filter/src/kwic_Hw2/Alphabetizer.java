package kwic_Hw2;

import java.util.ArrayList;
import java.util.Collections;

public class Alphabetizer extends Filter{

	public Alphabetizer(Pipe input_, Pipe output_) {
		super(input_, output_);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void transform() {
		ArrayList<String> alpha =new ArrayList<String>();
		String line;
		
		//get all the lines and sort them  and sent to pipe
		try {
			while ((line=input_.get().toString())!= "STOP351786548775GYUFABJFFAUYRJB"){
			
			alpha.add(line);
			
			}
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		Collections.sort(alpha, String.CASE_INSENSITIVE_ORDER);
		
		for(int i=0;i<alpha.size();i++){
			output_.put(alpha.get(i));
		}
		output_.put("STOP351786548775GYUFABJFFAUYRJB");
	}

}
