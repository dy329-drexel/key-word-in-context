package kwic_Hw2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;



public class Input extends Filter {

	private String fileName;
public Input(Pipe input_, Pipe output_) {
		
		super(input_, output_);
		
	}

	@Override
	protected void transform() {
		   
			//checking the parameter if its file or console
		//i added a keyword in when i finish reading from file or console so that i can stop all threads
			
		if(fileName.equals("console")){
			
			System.out.println("enter the consle input for kwic");
			
			
			Scanner input = new Scanner(System.in);
		    
		    String lineNew;

		    while (input.hasNextLine()) {
		        lineNew = input.nextLine();
		        if (lineNew.isEmpty()) {
		            break;
		        }
		     output_.put(lineNew);
			
			
		}
		    //just a keyword to let other filters know that input finished null was not working for me
		    output_.put("STOP351786548775GYUFABJFFAUYRJB");
		}
		else
		{
		   
		    FileReader fileReader;
			try {
				fileReader = new FileReader(new File(fileName));
				
				BufferedReader br = new BufferedReader(fileReader);

			
			    String line;
				// if no more lines the readLine() returns null
			    while ((line = br.readLine()) != null) {
			    	
			output_.put(line)     ;
			    }
			    
			   
			   output_.put("STOP351786548775GYUFABJFFAUYRJB");
			   
				
		}catch(Exception e){
			
		}
		}
	}


//getting input file name
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
