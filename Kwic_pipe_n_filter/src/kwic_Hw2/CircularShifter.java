package kwic_Hw2;

import java.util.Arrays;

public class CircularShifter extends Filter{
	

public CircularShifter(Pipe input_, Pipe output_) {
		
		super(input_, output_);
		
	}

	@Override
	protected void transform() {
	
		try {
			
	//take each line from pipe and shift it
			String line;
			while ((line=input_.get().toString())!= "STOP351786548775GYUFABJFFAUYRJB"){
			
			
		
				shift(line);
				
			}
			//keyword to stop
		output_.put("STOP351786548775GYUFABJFFAUYRJB");
		
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		
		}
	}
	
	
	
	
//method to circular shift
	private void shift(String line){

		String[] splited = line.split(" ");
		
		for(int i=0;i<splited.length-1;i++){
			
				String first = splited[0];
				System.arraycopy(splited, 1, splited, 0, splited.length-1);
				splited[splited.length-1] = first;
				String temp = Arrays.toString(splited);
			
				temp = temp.replaceAll(",","");
			
				temp = temp.substring(1, temp.length()-1);
				output_.put(temp);
			}
		}
}
