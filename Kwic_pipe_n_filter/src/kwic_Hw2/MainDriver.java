package kwic_Hw2;



public class MainDriver {
	
	
	//creating all filters
			static Input ip;
			static Converter cn;
			static CircularShifter cs;
			static Alphabetizer al;
			static Output op;
			
	public static void main(String args[]){
		
		//get arguments
	if(args.length<3)
		{	System.out.println("not enough arguments supplied .");
		
		System.out.println("three arguments needed 1.input file name or console  2. output file name  3.place for uppercase filter");
		return ;
	}
		
	//getting arguments for input file name and ouput file name
		String inputName=args[0].toString();
		String outputName=args[1].toString();
		int uppercase=Integer.parseInt(args[2]);
		
		
		//creating all the pipes 
	       Pipe pipe1 = null;
	        Pipe pipe2 = new PipeImpl();
	        Pipe pipe3 = new PipeImpl();
	        Pipe pipe4 = new PipeImpl();
	        Pipe pipe5 = new PipeImpl();
	        
	        //putting upper case converter based on value of argument passed
		switch(uppercase){
		case 1:       ip=new Input(pipe1, pipe2);
						 cn=new Converter(pipe2,pipe3);
					     cs=new CircularShifter(pipe3, pipe4);
						 al=new Alphabetizer(pipe4, pipe5);
						op=new Output(pipe5, null);
						break;
		case 2:          ip=new Input(pipe1, pipe2);
					    cs=new CircularShifter(pipe2, pipe3);
						cn=new Converter(pipe3,pipe4);
						al=new Alphabetizer(pipe4, pipe5);
						 op=new Output(pipe5, null);
						 break;
		case 3:			ip=new Input(pipe1, pipe2);
						cs=new CircularShifter(pipe2, pipe3);
						al=new Alphabetizer(pipe3, pipe4);
						cn=new Converter(pipe4,pipe5);
						op=new Output(pipe5, null);
						break;
						
		default:		 System.out.println("wrong location for uppercase converter");
						return;
	
			
			
			}
    	
 
        
   
     //setting input method/filename and output file name
     op.setOutputfile(outputName);
     ip.setFileName(inputName);
     
     //starting all threads of filters 
   ip.start();
   cs.start();
   al.start();
   cn.start();
   op.start();
   ip.stop();
   cs.stop();
   al.stop();
   cn.stop();
   op.stop();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
